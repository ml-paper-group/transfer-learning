import pickle
###

def out_pickel_vae(target_space, latent, network, layers_values, loss_training, model_parameters):
    '''
    Saving the results of variational autoencoder
    :param target_space: name of target space
    :param latent: list of embeddings
    :param network: trained network
    :param layers_values: ayer values per epoch
    :param loss_training: training loss per epoch
    :param model_parameters: hyperparameters used to train the model
    '''
    out_dic = {'latent': latent,
               'network': network,
               'layer_values': layers_values,
               'loss_training': loss_training,
               'model_parameters': model_parameters}

    with open('results/VAE_' + target_space + '_results.pickle', 'wb') as handle:
        pickle.dump(out_dic, handle, protocol=pickle.HIGHEST_PROTOCOL)