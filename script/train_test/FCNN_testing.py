import numpy as np
import torch
import torch.nn as nn
from torch.autograd import Variable
###

def FCNN_testing(network_test, features_testing, labels_testing, batch_size):
    '''
    Testing the trained neural network model
    :param network_test: trained network to be used for prediction
    :param features_testing: features of the test set interactions
    :param labels_testing: labels of the test set interactions
    :param batch_size: batch size
    :return: loss and accuracy of the model in the test set
    '''

    input_featnum = len(list(features_testing[0]))
    #
    batch_ids = [[iter*batch_size, min(len(labels_testing), (iter+1)*batch_size)] for iter in range(int(len(labels_testing)/batch_size)+1)]

    criterion = nn.NLLLoss()
    test_loss = 0
    correct = 0
    print('testing')
    with torch.no_grad():

        for batch_idx in range(len(batch_ids)):

            features_batch = [features_testing[feat_iter] for feat_iter in range(batch_ids[batch_idx][0], batch_ids[batch_idx][1])]
            features_expandlist = [np.float_(list(feature_list_iter)) for feature_list_iter in features_batch]

            labels_batch = list(np.array(labels_testing)[range(batch_ids[batch_idx][0], batch_ids[batch_idx][1])])#[labels_testing[iter] for iter in range(batch_ids[batch_idx][0], batch_ids[batch_idx][1])]

            data = torch.tensor(features_expandlist, dtype=torch.float)
            target = torch.tensor(labels_batch, dtype=torch.long)


            data, target = Variable(data), Variable(target)
            data = data.view(-1, input_featnum)
            net_out = network_test(data.float())

            test_loss += criterion(net_out, target).data.item()  # criterion(net_out, target).data[0]
            pred = net_out.data.max(1)[1]  # get the index of the max log-probability
            correct += pred.eq(target.data).sum()

        test_loss /= len(labels_testing)

    return 100. * correct.item() / len(labels_testing), test_loss