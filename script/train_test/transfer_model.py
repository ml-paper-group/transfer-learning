
from script.data_prep.FCNN_featureprep import dataset_gen
from script.nnet.FCNN_optimizer import model_init
from script.train_test.FCNN_training import FCNN_training
###

def transfer_train(layer_list, batch_size, optimizer_params,
                   interactions, chemical_feat, protein_feat,
                   cheminal_info, protein_info,
                   negative_ratio, test_set = [], output_classnum = 2,
                   testing_whiletraining = False,
                   score_cutoff_list = [[0.4 ,0.9] ,[0.5 ,0.9]], epoch_list = [4 ,4],
                   seed_dict = {'batch_shuffling' :42, 'negative_sampling' :42}):
    '''
    Implementing Filtering Transfer Machine
    :param layer_list: List of layer sizes
    :param batch_size: batch size
    :param optimizer_params: parameters to be used for optimization
    :param interactions: dataframe of chemical-protein interactions
    :param chemical_feat: list of features of chemical space
    :param protein_feat: list of features of protein space
    :param cheminal_info: dataframe of chemical space ot match the ids with the chemical annotations
    :param protein_info: dataframe of protein space ot match the ids with the protein annotations
    :param negative_ratio: ratio of number of negative data points to positive datapoints to be used for random negative sampling
    :param test_set: dataset generated for testing the model
    :param output_classnum: number of classes in the output variable
    :param testing_whiletraining: logical parameter (if True, testing is done per epoch to follow the trend of traingina and testing per epoch)
    :param score_cutoff_list: list of label confidence ranges to be used in Filtering Transfer Machine
    :param epoch_list: List of epochs for each step of Filtering Transfer Machine
    :param seed_dict: dictionary of random seeds to be used for 'batch_shuffling' and 'negative_sampling'
    :return: trained network, layer weights per epochs, loss and performance for training and testing
    '''

    input_featnum = len(chemical_feat[0] + protein_feat[0])
    net, optimizer = model_init(input_size = input_featnum,
                                num_layers = len(layer_list),
                                layers_size = layer_list,
                                output_size = output_classnum,
                                optimizer_params = optimizer_params)  # learning_rate=learn_rate, omentum=momentum
    #
    loss = []
    performance = []
    layers_values_list = []
    for transfer_iter in range(len(score_cutoff_list)):
        epoch_num  = epoch_list[transfer_iter]
        cutoffs = score_cutoff_list[transfer_iter]
        print('confidence cutoff{}'.format(cutoffs))

        print('transfer step {}'.format(transfer_iter +1))
        #
        labels_train, feature_ids = dataset_gen(interactions = interactions,
                                                chemical_info = cheminal_info,
                                                protein_info = protein_info,
                                                score_cut_positive = cutoffs,
                                                negative_ratio = negative_ratio,
                                                seed_shuffling = seed_dict['negative_sampling'])
        print('Training started')
        net, layers_values, loss_training, performance_training = FCNN_training(network_train = net,
                                                                                feature_ids = feature_ids,
                                                                                chemical_feat = chemical_feat,
                                                                                protein_feat = protein_feat,
                                                                                labels_training  = labels_train,
                                                                                batch_size = batch_size,
                                                                                epochs = epoch_num,
                                                                                optimizer = optimizer,
                                                                                seed_shuffling = seed_dict
                                                                                    ['batch_shuffling'],
                                                                                test_set = test_set,
                                                                                testing_whiletraining = testing_whiletraining)

        layers_values_list.append(layers_values)
        loss.append(loss_training)
        performance.append(performance_training)

    return net, layers_values_list, loss, performance
